<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\journal;

class JournalRepository extends EntityRepository {
     
    
    
     
   /**
     * @return journal[]
     */
     public function findfromOneMonth($monday, $sunday)
    {
        return $this->createQueryBuilder('journal')
               ->Where('journal.data BETWEEN :monday AND :sunday')
               ->setParameter('monday', $monday->format('Y-m-d'))
               ->setParameter('sunday', $sunday->format('Y-m-d'))
               ->getQuery()
            ->execute();
    }
    
   
     public function GetOnlyTen()
    {
        return $this->createQueryBuilder('journal')
            ->setMaxResults(10)
            ->getQuery()
            ->execute();
    }
     
}
