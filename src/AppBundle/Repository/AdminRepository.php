<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AdminRepository extends EntityRepository {
     
     public function createIsScientistQueryBuilder()
    {
        return $this->createQueryBuilder('article')
            ->andWhere('user.isScientist = :isScientist')
            ->setParameter('isScientist', true);
    }
     
}
