<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Article;
use AppBundle\Forms\commentForm;
use AppBundle\functions\Functions;
use AppBundle\Entity\comment;

class ArchiveJournalController extends Controller
{  
  /**
    * @Route("/journal/styczen", name="styczen")
    */
    public function journalstyczenAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
      
        $Styczen=new \DateTime('2017-01-01');
        $Luty=new \DateTime('2017-02-01');
       
          
        $styczen_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Styczen,$Luty);
    
        

        return $this->render('journal/journal-styczen.html.twig', array( 
            'notes' => $styczen_posty,
            'articles' => $reversed,  
        ));   
    } 
    
    /**
    * @Route("/journal/luty", name="luty")
    */
    public function journalLutyAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
        $Luty=new \DateTime('2017-02-01');
        $Marzec=new \DateTime('2017-03-01');
          
        $Luty_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Luty,$Marzec);
    
        

        return $this->render('journal/journal-luty.html.twig', array( 
            'notes' => $Luty_posty,
            'articles' => $reversed,  
        ));   
    } 
    /**
    * @Route("/journal/marzec", name="marzec")
    */
    public function journalMarzecAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
      
      
        $Marzec=new \DateTime('2017-03-01');
        $Kwiecień=new \DateTime('2017-04-01');
        
          
        $marzec_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Marzec,$Kwiecień);
    
        

        return $this->render('journal/journal-marzec.html.twig', array( 
            'notes' => $marzec_posty,
            'articles' => $reversed,  
        ));   
    
        
             }
             
   /**
    * @Route("/journal/kwiecien", name="kwiecien")
    */
    public function journalkwiecienAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
      
 
        $Kwiecień=new \DateTime('2017-04-01');
        $Maj=new \DateTime('2017-05-01');
      
          
        $kwiecien_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Kwiecień,$Maj);
    
        

        return $this->render('journal/journal-kwiecien.html.twig', array( 
            'notes' => $kwiecien_posty,
            'articles' => $reversed,  
        ));   
    } 
    
    /**
    * @Route("/journal/maj", name="maj")
    */
    public function journalmajAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
      
 
       
        $Maj=new \DateTime('2017-05-01');
        $Czerwiec=new \DateTime('2017-06-01');
      
          
        $Maj_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Maj,$Czerwiec);
    
        

        return $this->render('journal/journal-maj.html.twig', array( 
            'notes' => $Maj_posty,
            'articles' => $reversed,  
        ));   
    } 
    
      /**
    * @Route("/journal/czerwiec", name="czerwiec")
    */
    public function journalczerwiecAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
      
 
       
    
        $Czerwiec=new \DateTime('2017-06-01');
        $Lipiec=new \DateTime('2017-07-01');
      
          
        $czerwiec_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Czerwiec,$Lipiec);
    
        

        return $this->render('journal/journal-czerwiec.html.twig', array( 
            'notes' => $czerwiec_posty,
            'articles' => $reversed,  
        ));   
    } 
    
        /**
    * @Route("/journal/lipiec", name="lipiec")
    */
    public function journallipiecAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
      
 
       
    
        $lipiec=new \DateTime('2017-07-01');
        $Sierpien=new \DateTime('2017-08-01');
      
          
        $Lipiec_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($lipiec,$Sierpien);
    
        

        return $this->render('journal/journal-lipiec.html.twig', array( 
            'notes' => $Lipiec_posty,
            'articles' => $reversed,  
        ));   
    } 
    
        /**
    * @Route("/journal/sierpien", name="sierpien")
    */
    public function journalsierpienAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
      
 
       
    
     
     
        $Sierpień=new \DateTime('2017-08-01');
        $Wrzesień=new \DateTime('2017-09-01');
      
          
        $Sierpien_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Sierpień, $Wrzesień);
    
        

        return $this->render('journal/journal-sierpien.html.twig', array( 
            'notes' => $Sierpien_posty,
            'articles' => $reversed,  
        ));   
    } 
    
          /**
    * @Route("/journal/wrzesien", name="wrzesien")
    */
    public function journalwrzesienAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
        $Wrzesień=new \DateTime('2017-09-01');
        $Pazdziernik=new \DateTime('2017-10-01');
          
        $Wrzesien_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Wrzesień, $Pazdziernik);
    
        

        return $this->render('journal/journal-wrzesien.html.twig', array( 
            'notes' => $Wrzesien_posty,
            'articles' => $reversed  
        ));   
    } 
    
             /**
    * @Route("/journal/pazdziernik", name="pazdziernik")
    */
    public function journalpazdziernikAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
        $reversed = array_reverse($article_new);
         
        $Pazdziernik=new \DateTime('2017-10-01');
        $Listopad=new \DateTime('2017-11-01');
          
        $Pazdziernik_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Pazdziernik, $Listopad);
    
        

        return $this->render('journal/journal-pazdziernik.html.twig', array( 
            'notes' => $Pazdziernik_posty,
            'articles' => $reversed,  
        ));   
    } 
    
              /**
    * @Route("/journal/listopad", name="listopad")
    */
    public function journallistopadAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
        
        $Listopad=new \DateTime('2017-11-01');
        $Grudzien=new \DateTime('2017-12-01');
        $Listopad_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth( $Listopad, $Grudzien);
    
        

        return $this->render('journal/journal-listopad.html.twig', array( 
            'notes' => $Listopad_posty,
            'articles' => $reversed 
        ));   
    } 
    
              /**
    * @Route("/journal/grudzien", name="grudzien")
    */
    public function journalgrudzienAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->findall();
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
        $Grudzien=new \DateTime('2017-12-01');
        $Grudzien_koniec=new \DateTime('2017-12-31');
    
        $grudzien_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Grudzien,$Grudzien_koniec);
    
        

        return $this->render('journal/journal-grudzien.html.twig', array( 
            'notes' => $grudzien_posty,
            'articles' => $reversed,  
        ));   
    } 
     
}
