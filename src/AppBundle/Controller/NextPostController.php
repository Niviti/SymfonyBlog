<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Forms\commentForm2;
use AppBundle\Forms\commentForm;
use AppBundle\Entity\comment2;
use AppBundle\Entity\comment;



class NextPostController extends Controller {
  
    
     /**
     * @Route("/journal/{id}", name="journal_next")
     */
    public function journalNextAction(Request $request, $id)
    {
         $comment = new comment();
         $form = $this->createForm(commentForm::class, $comment);
         $form->handleRequest($request);
         if($form->isSubmitted()) {
        
            $em= $this->getDoctrine()->getManager();
            $journal_note = $em->getRepository('AppBundle:journal')
            ->findOneBy([
                'id' => $id
            ]);
            
            $comment = $form->getData();
            $comment->setnote($journal_note);
            $comment->setdata(new \DateTime());
            $em->persist($comment);
            $em->flush();
            
            return $this->redirectToRoute('journal_next',array(
                'id' =>$id
            ));
        }
            
            $em= $this->getDoctrine()->getManager();
            $note = $em->getRepository('AppBundle:journal')
            ->findOneBy([
                'id' => $id
            ]);
            
     
            
         $journalx= $em->getRepository('AppBundle:journal')
              ->findAll();
          
          $table_length = count($journalx);
            
          for($i =0; $i <$table_length ; $i++)
         { 
            if($journalx[$i]->getId() ==$id )
            {
                $story= $i;
                goto end; 
            }
        }
        end:
             
          unset($journalx[$story]);
          $journalx[$story]=null ;
          
          $journal_new2= array();
          
          $journal_length = count($journalx);
       
          $x=0;
          
          for($i =0; $i < $journal_length-1 ; $i++)
          {
              
              if($journalx[$x]==null)
              {   
                  $x++;
                  $journal_new2[$i] = $journalx[$x];
              }
               
               $journal_new2[$i] = $journalx[$x];
               $x++;
          }
        

          
       
          
          
         $journal_length = count($journal_new2);
         $journal_new= array();
         $j=0;
   
        
         for($i =0; $i < $journal_length ; $i++)
         {
             $number = $journal_length-5;
             
             if($i>$number)
             {
                 
                 $journal_new[$j] = $journal_new2[$i];
                 $j++;
             }
         }
        
        $reversed = array_reverse($journal_new);
        
      
        
        return $this->render('admin/journal/journal_next.html.twig', array( 
            'journal' => $note,
            'commentForm' => $form->createView(),
            'journalxs' => $reversed
        ));
    }
    
     /**
     * @Route("/article/{id}", name="article_next")
     */
    public function articleNextAction(Request $request, $id)
    {
         $comment = new comment2();
    
         $form = $this->createForm(commentForm2::class, $comment);
     
         $form->handleRequest($request);
         if($form->isSubmitted()) {
        
            $em= $this->getDoctrine()->getManager();
            $journal_note = $em->getRepository('AppBundle:Article')
            ->findOneBy([
                'id' => $id
            ]);
            
            $comment = $form->getData();
            $comment->setnote($journal_note);
            $comment->setdata(new \DateTime());
            $em->persist($comment);
            $em->flush();
            
            return $this->redirectToRoute('article_next', array(
                 'id' =>$id   
            ));
        }
            
            $em= $this->getDoctrine()->getManager();
            $note = $em->getRepository('AppBundle:Article')
            ->findOneBy([
                'id' => $id
            ]);
            
     
            
         $journalx= $em->getRepository('AppBundle:Article')
              ->findAll();
          
          $table_length = count($journalx);
            
          for($i =0; $i <$table_length ; $i++)
         { 
            if($journalx[$i]->getId() ==$id )
            {
                $story= $i;
                goto end; 
            }
        }
        end:
             
          unset($journalx[$story]);
          $journalx[$story]=null ;
          
          $journal_new2= array();
          
          $journal_length = count($journalx);
       
          $x=0;
          
          for($i =0; $i < $journal_length-1 ; $i++)
          {
              
              if($journalx[$x]==null)
              {   
                  $x++;
                  $journal_new2[$i] = $journalx[$x];
              }
               
               $journal_new2[$i] = $journalx[$x];
               $x++;
          }
         $journal_length = count($journal_new2);
         $journal_new= array();
         $j=0;
   
         for($i =0; $i < $journal_length ; $i++)
         {
             $number = $journal_length-5;
             
             if($i>$number)
             {
                 
                 $journal_new[$j] = $journal_new2[$i];
                 $j++;
             }
         }
        
      $reversed = array_reverse($journal_new);
        
     return $this->render('article/article_next.html.twig', array( 
            'article' => $note,
            'commentForm2' => $form->createView(),
            'articlesx' => $reversed
        ));
    }
    
    
}
