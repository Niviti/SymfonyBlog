<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Forms\MailForm;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {    
         $em = $this->getDoctrine()->getManager();
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new=array();
         
         $j=0;
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         $reversed = array_reverse($article_new);
               
         return $this->render('default/index.html.twig', array( 
             'article_new' =>  $reversed
          
            ));
    }
    
   
       
    /**
     * @Route("/About",name="About")
     */
    public function AboutAction()
    {  
     return $this->render('default/about.html.twig');
    }
    
    
    
    
   /**
     * @Route("/kontakt", name="Kontakt")
     */
    public function SendEmail(Request $request)
    {
    
        $form = $this->createForm(MailForm::class);
        
        $form->handleRequest($request);
        
         if ($form->isSubmitted() && $form->isValid())
         {
               $Email = $form['Email']->getData();
               $Temat = $form['Temat']->getData();
               $Tekst = $form['Tekst']->getData();
             
                $message = \Swift_Message::newInstance()
                ->setSubject($Temat)
                ->setFrom($Email)
                ->setTo('rafak@o2.pl')
                ->setBody($Tekst);
                
                    $this->addFlash('success',
                 sprintf('Udało się wysłać Maila!')
                    );
                
                
                $this->get('mailer')->send($message);
                
                return $this->render('default/Kontakt.html.twig', array(
              'MailForm' => $form->createView()
         ));
         }
        
        
         return $this->render('default/Kontakt.html.twig', array(
              'MailForm' => $form->createView()
         ));
    }
        
}
