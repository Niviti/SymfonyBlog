<?php

namespace AppBundle\Controller\admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File;
use AppBundle\Forms\ArticleForm;
use AppBundle\Entity\Article;
use AppBundle\functions\Functions;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class AdminArticleController extends Controller
{
    /**
     * 
     *@Route("/admin/article" ,name="article") 
     */
    public function Article()
    {
        $em = $this->getDoctrine()->getManager();
        
        $articles= $em->getRepository('AppBundle:Article')
          ->findall();
        
        return $this->render('admin/article/article.html.twig', array( 
            'articles' => $articles
        ));
    }
    
    
    
    
    /**
     * @Route("/admin/article/create", name="article-creat")
     */
    public function CreatArticle(Request $request)
    {    
        $Article = new Article();
    
        $form = $this->createForm(ArticleForm::class, $Article);
     
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $file = $Article->getPicture();
          
            $Functions = new Functions();
         
            $em = $this->getDoctrine()->getManager();
            $zapytanieX = $em->getRepository('AppBundle:article')  
            ->findAll();
            $ilosc_wierszy = count($zapytanieX);
            $fileName =  $ilosc_wierszy+1 ;
            $fileName2 =  "$fileName.jpg";
            $file->move(
                $this->getParameter('picture_directory'),
                $fileName2
            );
            
            $Article->setPicture($fileName2);
            $Article = $form->getData();
            
            $em->persist($Article);
            $em->flush();
            
            $this->addFlash('success',
                 sprintf('Udało się stowrzyć artykuł !')
                    );
            
            return $this->redirectToRoute('article');
        }
         
              return $this->render('admin/article/article-new.html.twig', [
            'ArticleForm' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/admin/article/{id}", name="article_edit")
     */
    public function EditArticle(Request $request, Article $Article)
    {
        $form = $this->createForm(ArticleForm::class, $Article);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $file = $Article->getPicture();
          
            $Functions = new Functions();
         
            $em = $this->getDoctrine()->getManager();
            $zapytanieX = $em->getRepository('AppBundle:Article')  
            ->findAll();
            $ilosc_wierszy = count($zapytanieX);
            $fileName =  $ilosc_wierszy+1 ;
            $fileName2 =  "$fileName.jpg";
            $file->move(
                $this->getParameter('picture_directory'),
                $fileName2
            );
            
            $Article->setPicture($fileName2);
            $Article = $form->getData();
            
            $em->persist($Article);
            $em->flush();
        
                $this->addFlash('success',
                 sprintf('Udało się edytować artykuł !')
                    );
            
           
         /**
          *    return $this->redirectToRoute('article', [
          *    'id' => $Article->getId() ]);
          */
                
            return $this->redirectToRoute('article');
}
                 
            return $this->render('admin/article/article-edit.html.twig', [
            'ArticleForm' => $form->createView()
        ]);
    }
    
 
    
   /**
     * @Route("/admin/article/delete/{id}", name="article_delete")
     * 
     */
    public function DeleteAction($id)
    {   
        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository('AppBundle:Article')
            ->findOneBy([
                'id' => $id
            ]);
        
           $this->addFlash('success',
                 sprintf('Udało się usunąć artykuł !')
                    );
        
        
        $em->remove($article);
        $em->flush();

         return $this->redirectToRoute('article');
    }
    
   /**
    * 
    * @Route("/admin/article/comments/{id}", name="comments_show2")
    */
    public function ShowComments2($id)
    {
        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository('AppBundle:Article')
            ->findOneBy([
                'id' => $id
            ]);
        
        
        return $this->render('admin/article/article_comment.html.twig', array(
            'article' => $article
        ));
    }
    
    
   /**
     * @Route("admin/article/{articleId}/comments/{comment2Id}", name="article_comments_remove2")
     */
    public function DeleteComments2($articleId, $comment2Id)
    {        
             $em= $this->getDoctrine()->getManager();
        
             $note=$em->getRepository('AppBundle:comment2')
            ->findOneBy([
                'id' => $comment2Id,
                'note' => $articleId
             
            ]);
                 
                     $this->addFlash('success',
                 sprintf('Udało się usunąć komentarz !')
                    );
             
             
                 $em->remove($note);
                 $em->flush();

       return $this->redirectToRoute('article');
    }
    
 }
