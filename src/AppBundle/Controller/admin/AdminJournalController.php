<?php


namespace AppBundle\Controller\admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File;
use AppBundle\Forms\JournalForm;
use AppBundle\Entity\journal;
use AppBundle\functions\Functions;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class AdminJournalController extends Controller {
 
   /**
     * 
     *@Route("/admin/journal" ,name="journal-admin") 
     */
    public function Journal()
    {
        $em = $this->getDoctrine()->getManager();
        
        $journals= $em->getRepository('AppBundle:journal')
          ->findall();
        
        return $this->render('admin/journal/admin-journal.html.twig', array( 
            'journals' => $journals
        ));
    }
    
    /**
     * @Route("/admin/journal/create", name="journal-creat")
     */
    public function CreatArticle(Request $request)
    {    
        $Journal = new journal();
    
        $form = $this->createForm(JournalForm::class, $Journal);
     
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $file = $Journal->getPicture();
         
            $em = $this->getDoctrine()->getManager();
            $zapytanieX = $em->getRepository('AppBundle:journal')  
            ->findAll();
            $ilosc_wierszy = count($zapytanieX);
            $fileName =  $ilosc_wierszy+1 ;
            $fileName2 =  "$fileName.jpg";
            $file->move(
                $this->getParameter('picture_journal_directory'),
                $fileName2
            );
            
            $Journal->setPicture($fileName2);
            $Journal = $form->getData();
            
            $em->persist($Journal);
            $em->flush();
            
            $this->addFlash('success',
                 sprintf('Udało się stowrzyć notatke !')
                    );
            
            return $this->redirectToRoute('journal-admin');
        }
         
              return $this->render('admin/journal/admin-journal-new.html.twig', [
            'JournalForm' => $form->createView()
        ]);
    }
    
     /**
     * @Route("/admin/journal/{id}", name="journal_edit")
     */
    public function EditArticle(Request $request, journal $journal)
    {
        $form = $this->createForm(JournalForm::class, $journal);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            
            $file = $journal->getPicture();
          
            $em = $this->getDoctrine()->getManager();
            $zapytanieX = $em->getRepository('AppBundle:journal')  
            ->findAll();
            $ilosc_wierszy = count($zapytanieX);
            $fileName =  $ilosc_wierszy+1 ;
            $fileName2 =  "$fileName.jpg";
            $file->move(
                $this->getParameter('picture_journal_directory'),
                $fileName2
            );
            
            $journal->setPicture($fileName2);
            $journal = $form->getData();
            
            $em->persist($journal);
            $em->flush();
        
                $this->addFlash('success',
                 sprintf('Udało się edytować notatke !')
                    );
            return $this->redirectToRoute('journal-admin');
}
                 
            return $this->render('admin/journal/admin-journal-edit.html.twig', [
            'JournalForm' => $form->createView()
        ]);
    }
    
    /**
     * @Route("/admin/journal/delete/{id}", name="journal_delete")
     * 
     */
    public function DeleteAction($id)
    {   
        $em = $this->getDoctrine()->getManager();

        $journal = $em->getRepository('AppBundle:journal')
            ->findOneBy([
                'id' => $id
            ]);
        
           $this->addFlash('success',
                 sprintf('Udało się usunąć artykuł !')
                    );
        
        
        $em->remove($journal);
        $em->flush();

         return $this->redirectToRoute('journal-admin');
    }
    
  /**
    * 
    * @Route("/admin/journal/comments/{id}", name="comments_show")
    */
    public function ShowComments($id)
    {
        $em = $this->getDoctrine()->getManager();

        $journal= $em->getRepository('AppBundle:journal')
            ->findOneBy([
                'id' => $id
            ]);
        
        
        return $this->render('admin/journal/journal_comment.html.twig', array(
            'journal' => $journal
        ));
    }
    
    
   /**
     * @Route("admin/journal/{journalId}/comments/{commentId}", name="article_comments")
     */
    public function DeleteComments2($journalId, $commentId)
    {        
             $em= $this->getDoctrine()->getManager();
         
             $note=$em->getRepository('AppBundle:comment')
            ->findOneBy([
                'id' => $commentId,
                'note' => $journalId
             
            ]);
        
                  $this->addFlash('success',
                 sprintf('Udało się usunąć komentarz !')
                    );
             
         
                 $em->remove($note);
                 $em->flush();

        return $this->redirectToRoute('journal-admin');
    }
    
    
}
