<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Article;
use AppBundle\Forms\commentForm;
use AppBundle\Forms\MailForm;
use AppBundle\functions\Functions;
use AppBundle\Entity\comment;


class JournalController extends Controller  {
   
   /**
     * @Route("/journal", name="journal")
     */
    public function journalAction()
    {
         $em= $this->getDoctrine()->getManager();
         $Notes= $em->getRepository('AppBundle:journal')
                ->GetOnlyTen();
         
       
         
         
         
         $articles = $em->getRepository('AppBundle:Article')  
                  ->findAll();
         $article_length = count($articles);
         $article_new= array();
         $j=0;
       
       
         for($i =0; $i < $article_length ; $i++)
         {
             $number = $article_length-4;
             if($i>$number)
             {
                 
                 $article_new[$j] = $articles[$i];
                 $j++;
             }
         }
         
         $reversed = array_reverse($article_new);
         
      
        $Styczen=new \DateTime('2017-01-01');
        $Luty=new \DateTime('2017-02-01');
        $Marzec=new \DateTime('2017-03-01');
        $Kwiecień=new \DateTime('2017-04-01');
        $Maj=new \DateTime('2017-05-01');
        $Czerwiec=new \DateTime('2017-06-01');
        $Lipiec=new \DateTime('2017-07-01');
        $Sierpień=new \DateTime('2017-08-01');
        $Wrzesień=new \DateTime('2017-09-01');
        $Pazdziernik=new \DateTime('2017-10-01');
        $Listopad=new \DateTime('2017-11-01');
        $Grudzien=new \DateTime('2017-12-01');
        $Grudzien_koniec=new \DateTime('2017-12-31');
          
        $styczen_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Styczen,$Luty);
        $Luty_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Luty,$Marzec);
        $Marzec_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Marzec,$Kwiecień);
        $Kwiecien_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Kwiecień,$Maj);
        $Maj_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Maj,$Czerwiec);
        $Czerwiec_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Czerwiec,$Lipiec);
        $Lipiec_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Lipiec,$Sierpień);
        $Sierpień_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Sierpień,$Wrzesień);
        $Wrzesień_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Wrzesień,$Pazdziernik);
        $Pazdziernik_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Pazdziernik,$Listopad);
        $Listopad_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Listopad,$Grudzien);
        $Grudzien_posty = $em->getRepository('AppBundle:journal')
               ->findfromOneMonth($Grudzien, $Grudzien_koniec);
        
         
         $a= count(($styczen_posty));
         $b= count(($Luty_posty));
         $c= count(($Marzec_posty));
         $d= count(($Kwiecien_posty));
         $e= count(($Maj_posty));
         $f= count(($Czerwiec_posty));
         $g= count(($Lipiec_posty));
         $h= count(($Sierpień_posty));
         $i= count(($Wrzesień_posty));
         $j= count(($Pazdziernik_posty));
         $k= count(($Listopad_posty));
         $l= count(($Grudzien_posty));
         
         $Archiwum = array($a,$b,$c,$d,$e,$f,$g,$h,$i,$j,$k,$l);
        
        return $this->render('admin/journal/journal.html.twig', array( 
            'notes' => $Notes,
            'articles' => $reversed,
            'Archiwum' => $Archiwum
            
        ));
     }
    
}
