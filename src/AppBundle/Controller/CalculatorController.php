<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Forms\CalculatorForm;

class CalculatorController extends Controller
{
    /**
     * @Route("/kalkulator", name="kalkulator")
     */
    public function indexAction(Request $request)
    {    
        $form = $this->createForm(CalculatorForm::class);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid())
        {
        /*
         *Tutaj dostajemy wartosc 
         */
         $Kapital = $form["Kapital"]->getData();
         $ATR = $form["ATR"]->getData();   
         /*
          *Obliczemy wartosc Szczebelka
          */
         $Szczebel = $ATR/4;
         $Zysk = $ATR+($ATR-$Szczebel)+($ATR-2*$Szczebel)+($ATR-3*$Szczebel);
         /*
          *Obliczamy wielkosc pozycji
          */
         $wartosc = $Kapital/$Zysk;
         $wielkoscpozycji = 4*$wartosc;
         
         $this->addFlash('success',
                 sprintf('Szczebel piramidki wynosi'. ' '. $Szczebel. ' '. 'Wartość wielkości pozycji wynosi' .' ' .$wielkoscpozycji)
                    );
        return $this->redirectToRoute('kalkulator');
        }
        
        return $this->render('default/calculator.html.twig' ,array( 
            'CalculatorForm' => $form->createView()
        ));
    }   
}

