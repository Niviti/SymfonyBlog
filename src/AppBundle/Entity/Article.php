<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\Table(name="Article")
 */
class Article {
   
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id;
   /**
     * @ORM\Column(type="string")
     */     
    private $title;
   
  /**
    * @ORM\Column(type="text")
    */     
    private $text;
   /**
     *@ORM\Column(type="string")
     *@Assert\NotBlank(message="Please, upload the product brochure as a jpg file.")
     *@Assert\File(
     *     maxSize = "4024k")
     */
    private $picture;
    
    
   /**
     * @ORM\OneToMany(targetEntity="comment2", mappedBy="note")
     */
    private $comments2;
    
    public function getPicture()
    {
        return $this->picture;
    }

    public function setPicture($picture)
    {
        $this->picture = $picture;
        return $this;
    }
    
    public function getTitle()
    {
        return $this->title; 
    }
    
    public function setTitle($title)
    { 
       $this->title= $title;
       return $title;
    }
    
   public function getText()
   {
       return $this->text;
   }
   
   public function setText($text)
   {
       $this->text = $text;
       return $this ;
   }
   
   public function getId()
   {
       return $this->id;
   }
   
     public function __construct() 
    {
        $this->comments2 = new ArrayCollection();
    }
    
        /**
     * @return ArrayCollection|comment2[]
     */
    public function getcomments2()
    {
        return $this->comments2;
    }
   
   
  
}
