<?php



namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="comment")
 */
class comment {
  
   /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
     protected $id;
 
 /**
   *@ORM\Column(type="string")
   */     
    private $author;
   
  /**
    *@ORM\Column(type="text")
    */     
    private $text; 
    
    
   /**
     * @Assert\NotBlank()
     * @ORM\Column(type="date")
     */
    private $data; 
    
   /**
     * @ORM\ManyToOne(targetEntity="journal", inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
     private $note;
    
    public function getAuthor()
    {
     return $this->author;   
    }
    
    public function setAuthor($Author)
    {
        return $this->author= $Author; 
    }
    
    public function gettext()
    {
     return $this->text;        
    }
    
    public function settext($text)
    {
        return $this->text=$text;
    }
    
    public function getdata()
    {
        return $this->data;
    }
    
    public function setdata(\DateTime $data = null)
    {
        return $this->data=$data;
    }
    
    public function setnote($note)
    {
        return $this->note= $note;
    }
    
    public function getid()
    {
        return $this->id;
    }
    
}
