<?php


namespace AppBundle\Entity;



use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class Users implements UserInterface  {
   
   /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
     
   /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @ORM\Column(type="string", unique=true)
     */
    private $email;
     
  /**
    *@ORM\Column(type="string")
    */
    private $Password;
    
    
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function setPassword($Password)
    {
        $this->Password = $Password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
    
      // needed by the security system
    public function getUsername()
    {
        return $this->email;
    }
    
  
     public function getRoles()
    {
        return ['ROLE_ADMIN'];
    }
    
    public function getSalt()
    {
      return null ;   
    }
    
    
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }
    
     public function getPassword()
    {
        return $this->Password;
    }

    
}